///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file short.c
/// @version 1.0
///
/// Print the characteristics of the "short", "signed short" and "unsigned short" datatypes.
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "short.h"


///////////////////////////////////////////////////////////////////////////////
/// short

/// Print the characteristics of the "short" datatype
void doShort() {
}


/// Print the overflow/underflow characteristics of the "short" datatype
void flowShort() {
}


///////////////////////////////////////////////////////////////////////////////
/// signed short

/// Print the characteristics of the "signed short" datatype
void doSignedShort() {
}


/// Print the overflow/underflow characteristics of the "signed short" datatype
void flowSignedShort() {
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned short

/// Print the characteristics of the "unsigned short" datatype
void doUnsignedShort() {
}

/// Print the overflow/underflow characteristics of the "unsigned short" datatype
void flowUnsignedShort() {
}

