///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file char.c
/// @version 1.0
///
/// Print the characteristics of the "char", "signed char" and "unsigned char" datatypes.
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "char.h"


///////////////////////////////////////////////////////////////////////////////
/// char

/// Print the characteristics of the "char" datatype
void doChar() {
   printf(TABLE_FORMAT, "char", CHAR_BIT, CHAR_BIT/8, CHAR_MIN, CHAR_MAX);
}


/// Print the overflow/underflow characteristics of the "char" datatype
void flowChar() {
   char overflow = CHAR_MAX;
   overflow++;
   printf("char overflow:  %d + 1 becomes %d\n", CHAR_MAX, overflow);

   char underflow = CHAR_MIN;
   underflow--;
   printf("char underflow:  %d - 1 becomes %d\n", CHAR_MIN, underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// signed char

/// Print the characteristics of the "signed char" datatype
void doSignedChar() {
   printf(TABLE_FORMAT, "signed char", CHAR_BIT, CHAR_BIT/8, SCHAR_MIN, SCHAR_MAX);
}


/// Print the overflow/underflow characteristics of the "signed char" datatype
void flowSignedChar() {
   signed char overflow = SCHAR_MAX;
   overflow++;
   printf("signed char overflow:  %d + 1 becomes %d\n", SCHAR_MAX, overflow);

   signed char underflow = SCHAR_MIN;
   underflow--;
   printf("signed char underflow:  %d - 1 becomes %d\n", SCHAR_MIN, underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned char

/// Print the characteristics of the "unsigned char" datatype
void doUnsignedChar() {
   printf(TABLE_FORMAT, "unsigned char", CHAR_BIT, CHAR_BIT/8, 0, UCHAR_MAX);  // See the comment next to UCHAR_MAX in limits.h
}

/// Print the overflow/underflow characteristics of the "unsigned char" datatype
void flowUnsignedChar() {
   unsigned char overflow = UCHAR_MAX;
   overflow++;
   printf("unsigned char overflow:  %d + 1 becomes %d\n", UCHAR_MAX, overflow);

   unsigned char underflow = 0;  // Note:  See limits.h UCHAR_MAX... there's no UCHAR_MIN, it's 0.
   underflow--;
   printf("unsigned char underflow:  %d - 1 becomes %d\n", 0, underflow);
}

