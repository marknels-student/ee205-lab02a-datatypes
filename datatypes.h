///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file datatypes.h
/// @version 1.0
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#define TABLE_HEADER1 "Datatype      bits bytes Minimum Maximum\n"
#define TABLE_HEADER2 "------------- ---- ----- ------- -------\n"
#define TABLE_FORMAT "%-13s %4d %5d %7d %7d\n"

